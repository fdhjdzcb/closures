let array = [5, 3, 1, 2, 4]

// Сортировка в порядке возрастания
let sortedAscending = array.sorted(by: <)
print("Сортировка по возрастанию: \(sortedAscending)")

// Сортировка в порядке убывания
let sortedDescending = array.sorted(by: >)
print("Сортировка по убыванию: \(sortedDescending)")

var friendsNames = [String]()

func addFriendToArray(name: String) {
    friendsNames.append(name)
    friendsNames.sort(by: {$0.count < $1.count})
}

addFriendToArray(name: "Evgeniy")
addFriendToArray(name: "Amir")
addFriendToArray(name: "Peter")
addFriendToArray(name: "Ann")

print("Имена, отсортированные по длине:", friendsNames)

var dictionary: [Int : String] = [:]
for name in friendsNames {
    dictionary[name.count] = name
}

func getValueByKey(dict: [Int : String], key: Int) {
    print("key:", key, ", value:", dictionary[key, default: "Unknown"])
}
getValueByKey(dict: dictionary, key: 4)

var stringArray = [String]()
var intArray = [Int]()

func validateArrays(ints: [Int], strs: [String]) {
    if intArray.isEmpty {
        intArray.append(0)
    }
    if stringArray.isEmpty {
        stringArray.append("Empty")
    }
    print(intArray, stringArray, separator: ", ")
}
validateArrays(ints: [1, 2, 3], strs: ["A"])